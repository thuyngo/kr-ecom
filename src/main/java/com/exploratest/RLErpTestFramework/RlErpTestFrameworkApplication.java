package com.exploratest.RLErpTestFramework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class RlErpTestFrameworkApplication {
	public static void main(String[] args) {
		SpringApplication.run(RlErpTestFrameworkApplication.class, args);
	}

	@RequestMapping(value = "/")
	public String hello() {
		return "Ralph Lauren ERP System test framework";
	}
}
