package com.exploratest.functionalTest.framework.core;

import com.exploratest.functionalTest.framework.utils.ConfigUtil;
import com.exploratest.functionalTest.framework.utils.DBUtil;
import com.exploratest.functionalTest.framework.utils.ExcelReader;
import com.exploratest.functionalTest.framework.utils.SysUtil;
import com.exploratest.functionalTest.modules.SSGCancelData.SSGCancelData;
import com.exploratest.functionalTest.modules.SSGOrder.SSGOrder;
import com.exploratest.functionalTest.modules.SSGReturn.SSGReturn;
import com.exploratest.functionalTest.modules.System.ModuleMenu;
import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.model.StepResult;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestRunner;
import org.testng.annotations.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Listeners({ TestEngineListener.class })
public class TestCasesDriver {

    private static Logger logger = LoggerFactory.getLogger(TestCasesDriver.class);
    private static final String TEST_CASES_SHEET="Test Cases";
    private static final String TEST_STEPS_SHEET="Test Steps";
    private final static String DRIVER_NAME = "db.jdbc.driver.class";
    private final static String CONN_STR = "db.connection.string";
    private final static String DB_ID = "db.id";
    private final static String DB_PW = "db.pw";
    private enum ExecStatus { SUCCESS, FAIL, NOT_RUN }

    private final static String MODULES_PACKAGE = "com.exploratest.functionalTest.modules.";

    private final static Map<String, String> MODULE_CLASS_MAP = new HashMap<String, String>() {
        {
            //SGG Return
            put(SSGReturn.class.getSimpleName(), SSGReturn.class.getName());
            //SGG Cancel Data
            put(SSGCancelData.class.getSimpleName(), SSGCancelData.class.getName());
            //SGG Order
            put(SSGOrder.class.getSimpleName(), SSGOrder.class.getName());
        }
    };

    private ExcelReader xlsReader;
    private RemoteWebDriver webDriver;
    private ModuleMenu moduleMenu;
    //private Toolbar toolbar;

    private DBUtil dbUtil = null;
    private HashMap<String, ExecStatus> execStatusMap = new HashMap<>();

    @BeforeClass
    public void setupBeforeClass(ITestContext context) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        String dateStr = dateFormat.format(new Date());
        TestRunner runner = (TestRunner) context;
        runner.setOutputDirectory(String.format("%s/%s", ConfigUtil.INSTANCE.getTestNGDir(), dateStr));

        System.setProperty(ConfigUtil.INSTANCE.PROP_KEY_ALLURE_RESULT_DIR, ConfigUtil.INSTANCE.getAllureResultDir());
        if (!setupDriver()) {
            Assert.fail("failed to setup driver");
        }

        if (ConfigUtil.INSTANCE.getDBAccess().equals("true")) {
            this.dbUtil = new DBUtil(System.getProperty(DRIVER_NAME), System.getProperty(CONN_STR),
                    System.getProperty(DB_ID), System.getProperty(DB_PW));
        } else {
            this.dbUtil = null;
            logger.info("DBUtil is not being used");
        }
    }

    private boolean setupDriver() {
        logger.info("setting up driver");
        try {
            switch (ConfigUtil.INSTANCE.getBrowserType().toLowerCase()) {
                case BrowserType.CHROME:
                    webDriver = chromeSetup();
                    break;
                case BrowserType.IE:
                case BrowserType.IEXPLORE:
                default:
                    webDriver = ieSetup();
                    break;
                // default:
                //     throw new UnsupportedOperationException(String.format("Unknown browser type %s", ConfigUtil.INSTANCE.getBrowserType()));
            }
            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
            webDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

            moduleMenu = new ModuleMenu(webDriver);
            //toolbar = new Toolbar(webDriver);
        } catch (Exception e) {
            logger.error("failed to setup Driver ", e);
            return false;
        }
        return true;
    }

    @AfterClass
    public void tearDownAfterClass(ITestContext context) {
        if (ConfigUtil.INSTANCE.getDBAccess().equals("true")) {
            this.dbUtil.CloseConnection();
        }

        if (this.webDriver != null) {
            this.webDriver.close();
            this.webDriver.quit();
        }
    }

    @BeforeMethod
    public void setUp() {

    }

    @AfterMethod
    public void tearDown(ITestResult result) {

    }

    @DataProvider(name="getTestSet")
    public Iterator<Object[]> getTestSet() {
        String testSet = ConfigUtil.INSTANCE.getTestSet();
        logger.info("getting test set from {}", testSet);
        xlsReader = new ExcelReader(testSet);
        try {
            return xlsReader.getRows(TEST_CASES_SHEET, "Y", 2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Iterator <Object[]> getTestSteps(String testStepSheet, String caseID) {
        xlsReader = new ExcelReader(ConfigUtil.INSTANCE.getTestSet());
        try {
            return xlsReader.getRows(testStepSheet, caseID, 0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test(dataProvider = "getTestSet") //, dataProviderClass=TestCasesDriver.class)
    public void runTestCase(List<String> row, ITestContext ctx) {
        logger.info(String.format("row: \"%s\"", String.join("\",\"", row)));

        String testCaseId = row.get(0);
        String testCaseTitle = row.get(1);
        String testStepsSheet = TEST_STEPS_SHEET;

        AllureLifecycle lifecycle = Allure.getLifecycle();
        String uuid = UUID.randomUUID().toString();
        lifecycle.startTestCase(uuid);
        lifecycle.updateTestCase(u -> {
            u.setFullName(testCaseId);
            u.setDescription(testCaseTitle);
        });

        if (row.size() >= 4 && !row.get(3).trim().isEmpty()) {
            testStepsSheet = row.get(3);
        }
        if (row.size() >= 5 && !row.get(4).trim().isEmpty()) {
            String column4 = row.get(4);
            if (execStatusMap.get(column4) == null ||
                    execStatusMap.get(column4) == ExecStatus.FAIL ||
                    execStatusMap.get(column4) == ExecStatus.NOT_RUN) {
                logger.info(String.format("Dependent case failed or have not run, thus skip case %s", testCaseId));
                Allure.addDescription(String.format("Dependent case failed or have not run, thus skip case %s\n", testCaseId));
                execStatusMap.put(testCaseId, ExecStatus.NOT_RUN);
                //ITestResult itr = Reporter.getCurrentTestResult();
                //itr.setStatus(ITestResult.SKIP);
                //Reporter.setCurrentTestResult(itr);
                return;
            }
        }

        logger.info(String.format("get test steps: %s", testStepsSheet));
        Iterator<Object[]> steps = getTestSteps(testStepsSheet, testCaseId);
        if (steps == null || !steps.hasNext()) {
            logger.info("no steps to run");
            execStatusMap.put(testCaseId, ExecStatus.NOT_RUN);
            return;
        }

        for (Iterator<Object[]> iterator = steps; iterator.hasNext();) {
            Object obj = iterator.next()[0];

            List<String> stepArgs = (List<String>) obj;

            String caseId = stepArgs.get(0);
            String caseStepNum = stepArgs.get(1);
            // String moduleName = stepArgs.get(2);
            // String actionName = stepArgs.get(3);

            try {
                runStep(stepArgs, caseId, ctx); //if any of the step in a test case failed, mark the case as failed
                execStatusMap.put(testCaseId, ExecStatus.SUCCESS);
            } catch (AssertionError e) {
                String errorMessageToLog = String.format("TEST CASE FAILED: (%s) %s, step: %s; message: %s", caseId, testCaseTitle, caseStepNum, e.getMessage());
                logger.error(errorMessageToLog, e);
                //Allure.addDescription(String.format("TEST CASE FAILED: (%s) %s, step: %s", caseId, testCaseTitle, caseStepNum));
                execStatusMap.put(testCaseId, ExecStatus.FAIL);
                this.webDriver.close();
                this.webDriver.quit();
                Assert.fail(errorMessageToLog, e);
            } finally {
                logger.info(String.format("case: (%s) %s, step: %s done.", caseId, testCaseTitle, caseStepNum));
                //recreate driver if killed
                if (webDriver == null || webDriver.getSessionId() == null) {
                    this.setupDriver();
                }
            }
        }
        lifecycle.stopTestCase(uuid);
        if (webDriver != null) {
            this.webDriver.close();
            this.webDriver.quit();
        }
        this.setupDriver();
    }

    private boolean runStep(List<String> stepArgs, String testCaseDesc, ITestContext ctx) {
        AllureLifecycle lifecycle = Allure.getLifecycle();
        String uuid = UUID.randomUUID().toString();
        StepResult result = new StepResult();

        String caseId = stepArgs.get(0);
        String caseStepNum = stepArgs.get(1);
        String moduleName = stepArgs.get(2).trim();
        String actionName = stepArgs.get(3).trim();
        String actionDescription = stepArgs.get(4).trim();

        result.withName(String.format("%s -> %s", actionName, actionDescription));
        lifecycle.startStep(uuid, result);

        if (ConfigUtil.INSTANCE.getScreenShotBeforeStep()) {
            this.addScreenToReport(lifecycle, String.format("start %s", actionName), "image/png", ".png");
        }

        Class<?> testModule;
        Object moduleInstance = null;
        try {
            testModule = getModule(moduleName);
            try {
                Constructor<?> construct = testModule.getConstructor(WebDriver.class, ModuleMenu.class);
                moduleInstance = construct.newInstance(webDriver, moduleMenu);
            } catch (InstantiationException e) {
                logger.error(String.format("failed to create instance of %s", moduleName), e);
            } catch (NoSuchMethodException e) {
                String ctorNotFoundMessage = String.format("Constructor for %s not found", testModule.getName());
                logger.error(ctorNotFoundMessage);
                Assert.fail(ctorNotFoundMessage);
                return false;
            }

            Optional<Method> mth = Arrays.stream(testModule.getMethods()).filter(m -> m.getName().equals(actionName)).findFirst();
            if (!mth.isPresent()) {
                String methodNotFoundMessage = String.format("method %s not found!", actionName);
                Assert.fail(methodNotFoundMessage);
                logger.error(methodNotFoundMessage);
                return false;
            }
            Method method = mth.get();
            //excel cell cannot be null
            List<String> args = new ArrayList<>();
            for (int i = 7; i < 13; i++) {
                if (i == 8) {
                    continue;
                }
                if (stepArgs.size() > i) {
                    this.addArgs(args, stepArgs.get(i));
                } else {
                    this.addArgs(args, "");
                }
            }

            // for catering varargs
            Class[] parameterTypes = method.getParameterTypes();
            String[] varargs = null;
            int varargsIndex;
            for ( varargsIndex = parameterTypes.length - 1; varargsIndex >= 0; varargsIndex--) {
                if (parameterTypes[varargsIndex].isArray()) {
                    varargs = new String[args.size() - varargsIndex];
                    for (int j = 0; j < varargs.length; j++) {
                        varargs[j] = args.get(j + varargsIndex);
                    }
                    break;
                }
            }

            Object[] realArgs = new Object[parameterTypes.length];
            for (int i = 0; i < parameterTypes.length; i++) {
                if (i == varargsIndex) {
                    realArgs[i] = varargs;
                    break;
                }
                realArgs[i] = args.get(i);
            }

            method.invoke(moduleInstance, realArgs);
            return true;
        } catch (IllegalAccessException | IllegalArgumentException e) {
            logger.error("Wrong argument or argument count", e);
            result.withDescription(e.getMessage());
            Assert.fail(e.getMessage());
        } catch (InvocationTargetException e) {
            logger.error(String.format("failed to invoke or error in the method: %s", actionName), e);
            result.withDescription(e.toString());
            Assert.fail(e.getMessage());
        } catch (SecurityException e) {
            logger.error("security exception", e);
            result.withDescription(e.getMessage());
            Assert.fail(String.format("Not enough privilege to access method: %s", actionName));
        } catch (TimeoutException e) {
            result.withDescription(e.getMessage());
            logger.error("timeout waiting for element", e);
            Assert.fail(String.format("timeout waiting for element on test step: %s", actionName));
        } catch (NoSuchElementException e) {
            result.withDescription(e.getMessage());
            logger.error("Web element not found", e);
            Assert.fail("Web element not found", e);
        } catch (Exception e) {
            logger.error("exception ", e);
            Assert.fail("failed to run step", e);
        }
        finally {
            if (ConfigUtil.INSTANCE.getScreenShotAfterStep()) {
                this.addScreenToReport(lifecycle, String.format("end %s", actionName), "image/png", ".png");
            }
            lifecycle.stopStep();
        }
        return false;
    }

    private void addScreenToReport(AllureLifecycle lifecycle, String description, String imageType, String extension) {
        try {
            lifecycle.addAttachment(description, imageType, extension, this.getScreenShot());
        } catch (IllegalMonitorStateException ex) {
            logger.warn(String.format("screen not yet ready to take screenshot at end of step: %s", description));
        }
    }

    private Class<?> getModule(String uniqueModuleName) {
        Class<?> module;

        String moduleName = uniqueModuleName.trim();

        try {
            module = Class.forName(String.format("%s%s", MODULES_PACKAGE, moduleName));
            return module;
        } catch (ClassNotFoundException e) {
            logger.trace(String.format("not found in package %s", MODULES_PACKAGE));
        }

//        try {
//            module = Class.forName(String.format("%sproductInfoMgmt.%s", MODULES_PACKAGE, moduleName));
//            return module;
//        } catch (ClassNotFoundException e) {
//            logger.trace(String.format("not found in package %sproductInfoMgmt.%s", MODULES_PACKAGE, moduleName));
//        }
//        //Thuynt add this code for inventory module
//        try {
//            module = Class.forName(String.format("%sinventoryMgmt.%s", MODULES_PACKAGE, moduleName));
//            return module;
//        } catch (ClassNotFoundException e) {
//            logger.trace(String.format("not found in package %sinventoryMgmt.%s", MODULES_PACKAGE, moduleName));
//        }
//        //Thuynt add this code for finance module
//        try {
//            module = Class.forName(String.format("%sfinanceMgmt.%s", MODULES_PACKAGE, moduleName));
//            return module;
//        } catch (ClassNotFoundException e) {
//            logger.trace(String.format("not found in package %sfinanceMgmt.%s", MODULES_PACKAGE, moduleName));
//        }
//        try {
//            module = Class.forName(String.format("%ssalesAndMarketing.%s", MODULES_PACKAGE, moduleName));
//            return module;
//        } catch (ClassNotFoundException e) {
//            logger.trace(String.format("not found in package %ssalesAndMarketing.%s", MODULES_PACKAGE, moduleName));
//        }
        try {
            module = Class.forName(MODULE_CLASS_MAP.get(uniqueModuleName));
            return module;
        } catch (ClassNotFoundException e) {
            logger.trace(String.format("Cannot found suitable package for module %s", uniqueModuleName));
        }
        return null;
    }

    private List<String> addArgs(List<String> strList, String params) {
        if (!params.trim().isEmpty()) {
            for (String str : params.split("\\s?\\|\\s?+")) {
                String trimmed = str.trim();
                // if (trimmed.length() > 0) {
                    logger.info(String.format("argument to add: %s", trimmed));
                    strList.add(trimmed); //result message
                // }
            }
        }
        return strList;
    }

    private InternetExplorerDriver ieSetup() {
        try {
            SysUtil.killProcess("IEDriverServer.exe");
        } catch (Exception e) {
            logger.error("failed to kill IEServerDriver ", e);
            return null;
        }

        InternetExplorerDriver ieDriver = null;
        InternetExplorerOptions options = new InternetExplorerOptions()
                .destructivelyEnsureCleanSession()
                .requireWindowFocus()
                .ignoreZoomSettings()
                .setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);

        options.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
        options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        options.setCapability(InternetExplorerDriver.LOG_FILE, ConfigUtil.INSTANCE.getIELog());
        options.setCapability(InternetExplorerDriver.LOG_LEVEL, ConfigUtil.INSTANCE.getIELogLevel());

        if (!ConfigUtil.INSTANCE.getProperty(CapabilityType.PROXY).isEmpty()) {
            Proxy proxy = new Proxy();
            proxy.setHttpProxy(ConfigUtil.INSTANCE.getProperty(CapabilityType.PROXY));
            options.setProxy(proxy);
        }

        System.setProperty(ConfigUtil.INSTANCE.PROP_KEY_IE_DRIVER, ConfigUtil.INSTANCE.getIEDriver());

        try {
            ieDriver = new InternetExplorerDriver(options);
            ieDriver.manage().window().maximize();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Failed to setup IE: ", e);
            throw e;
        }
        logger.info(String.format("ieDriver setup: %s", ieDriver.toString()));
        return ieDriver;
    }

    private ChromeDriver chromeSetup() {
        try {
            if (SystemUtils.IS_OS_WINDOWS) {
                SysUtil.killProcess("chromedriver.exe");
            } else {
                SysUtil.killProcess("chromedriver");
            }
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
        ChromeDriver chromeDriver;
        ChromeOptions options = new ChromeOptions();

        options.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        options.setCapability("chrome.switches", Arrays.asList("--incognito"));


        //options.setBinary(cfgFile.getProperty("chrome.binary.path"));
        System.setProperty(ConfigUtil.INSTANCE.PROP_KEY_CHROME_LOG, ConfigUtil.INSTANCE.getChromeLog());
        System.setProperty("webdriver.chrome.verboseLogging", "true");
        System.setProperty(ConfigUtil.INSTANCE.PROP_KEY_CHROME_DRIVER,ConfigUtil.INSTANCE.getChromeDriver());

        //Thuynt add directory
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put(ConfigUtil.INSTANCE.DOWNLOAD_DIRECTORY, ConfigUtil.INSTANCE.getDirectoryDownload());
        options.setExperimentalOption("prefs", chromePrefs);

        options.setCapability(CapabilityType.BROWSER_NAME, "chrome");
        options.addArguments("start-maximized", "disable-popup-blocking");

        options.addArguments("--dns-prefetch-disable");
        options.addArguments("--force-device-scale-factor=1");
        options.addArguments("--disable-gpu");
        options.addArguments("disable-features=NetworkService");
        //Thuynt test
        //options.addArguments("--headless");

        if (!ConfigUtil.INSTANCE.getProperty(CapabilityType.PROXY).isEmpty()) {
            Proxy proxy = new Proxy();
            proxy.setHttpProxy(ConfigUtil.INSTANCE.getProperty(CapabilityType.PROXY));
            options.setProxy(proxy);
        }

        try {
            chromeDriver = new ChromeDriver(options);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Failed to setup Chrome: ", e);
            throw e;
        }
        logger.info(String.format("chromeDriver setup: %s", chromeDriver.toString()));
        return chromeDriver;
    }

    protected byte[] getScreenShot() {
        if (webDriver == null) {
            logger.error("driver is empty, unable to take screenshot");
            return null;
        }
        return webDriver.getScreenshotAs(OutputType.BYTES);
    }
}
