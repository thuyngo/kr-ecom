package com.exploratest.functionalTest.framework.core;

import com.exploratest.functionalTest.framework.utils.ConfigUtil;
import io.qameta.allure.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestEngineListener implements ITestListener {

    private static Logger logger = LoggerFactory.getLogger(TestEngineListener.class);

    @Override
    public void onTestStart(ITestResult result) {

    }

    /**
     * Invoked each time a test succeeds.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     * @see ITestResult#SUCCESS
     */
    @Override
    public void onTestSuccess(ITestResult result) {
//        logger.debug("listener got test success");
//        if (!ConfigUtil.INSTANCE.getScreenShotAfterStep())
//            return;
//        Object testClass = result.getInstance();
//        this.saveScreenShot((TestCasesDriver) testClass);
//        this.saveMessage(getMethdodName(result) + " passed and screenshot taken");

    }

    /**
     * Invoked each time a test fails.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     * @see ITestResult#FAILURE
     */
    @Override
    public void onTestFailure(ITestResult result) {
//        logger.debug("listener got test failed");
//        if (!ConfigUtil.INSTANCE.getScreenShotBeforeStep())
//            return;
//        Object testClass = result.getInstance();
//        this.saveScreenShot((TestCasesDriver) testClass);
//        this.saveMessage(getMethdodName(result) + " failed and screenshot taken");

    }

    /**
     * Invoked each time a test is skipped.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     * @see ITestResult#SKIP
     */
    @Override
    public void onTestSkipped(ITestResult result) {

    }

    /**
     * Invoked each time a method fails but has been annotated with
     * successPercentage and this failure still keeps it within the
     * success percentage requested.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     * @see ITestResult#SUCCESS_PERCENTAGE_FAILURE
     */
    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    /**
     * Invoked after the test class is instantiated and before
     * any configuration method is called.
     *
     * @param context
     */
    @Override
    public void onStart(ITestContext context) {
    }

    /**
     * Invoked after all the tests have run and all their
     * Configuration methods have been called.
     *
     * @param context
     */
    @Override
    public void onFinish(ITestContext context) {

    }

    private static String getMethdodName(ITestResult testResult) {
        return testResult.getMethod().getConstructorOrMethod().getName();
    }

//    @Attachment(value = "Page screenshot", type = "image/png")
//    public byte[] saveScreenShot(TestCasesDriver driver) {
//        return driver.getScreenShot();
//    }
//
//    @Attachment(value = "{0}", type = "text/plain")
//    public static String saveMessage(String message) {
//        return message;
//    }
}
