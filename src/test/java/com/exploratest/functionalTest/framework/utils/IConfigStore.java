package com.exploratest.functionalTest.framework.utils;

public interface IConfigStore {
    String getProperty(String key, String defaultVal);
}