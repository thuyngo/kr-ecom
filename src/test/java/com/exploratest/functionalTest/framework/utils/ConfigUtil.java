package com.exploratest.functionalTest.framework.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Properties;

class PropFileConfigStore implements IConfigStore {
    private Properties cfgFile;

    PropFileConfigStore(String cfgFileName) {
        Logger logger = LoggerFactory.getLogger(PropFileConfigStore.class);
        try {
            FileInputStream fs = new FileInputStream(cfgFileName);
            cfgFile = new Properties();
            cfgFile.load(fs);
            logger.info("config file: " + cfgFileName);
        } catch (Exception e) {
            logger.error("failed to load config file: " + cfgFileName, e);
        }
    }

    public String getProperty(String key, String defaultVal) {
        return cfgFile.getProperty(key, defaultVal);
    }
}

class XlsConfigStore implements IConfigStore {
    private ExcelReader excelReader;
    private int xlRowCount = 0;
    Logger logger = LoggerFactory.getLogger(XlsConfigStore.class);

    XlsConfigStore(String cfgFileName) {
        try {
            excelReader = new ExcelReader(cfgFileName);
            xlRowCount = excelReader.getRowCount(xlRowCount);
            logger.info("config file: " + cfgFileName + ", rows: " + xlRowCount);
        } catch (Exception e) {
            logger.error("failed to load config file: " + cfgFileName, e);
        }
    }

    public String getProperty(String key, String defaultVal) {
        int i = 0;
        String value = defaultVal;
        do {
            try {
                String xlKey = excelReader.getCellData(0, 0, i);
                if (xlKey != null && key.equalsIgnoreCase(xlKey.trim())) {
                    value = excelReader.getCellData(0, 1, i);
                    i = xlRowCount;
                }
            } catch (Exception e) {
                logger.error("Failed to get value for key: " + key);
                value = defaultVal;
            }
            i++;
        } while (i<xlRowCount);
        return value;
    }
}

//Singleton system config util to manage application specific config and other system config
public enum ConfigUtil {
    INSTANCE;

    private IConfigStore configStore;
    private boolean screenshotBeforeStep = false;
    private boolean screenshotAfterStep = false;
    private long execDelay;
    private String CFG_FILE_PATH = System.getProperty("test_config_file");

    public final String PROP_KEY_TEST_SET = "testSet";
    public final String PROP_KEY_HOME_URL = "homeURL";
    public final String PROP_KEY_BASE_URL = "baseURL";
    public final String PROP_KEY_TESTNG_DIR = "testNGDir";
    public final String PROP_KEY_DB_ACCESS = "dbAccess";
    public final String PROP_KEY_SCREENSHOT_BEFORE_STEP = "screenshotOnFail";
    public final String PROP_KEY_SCREENSHOT_AFTER_STEP = "screenshotOnPass";

    public final String PROP_KEY_ALLURE_RESULT_DIR = "allure.results.directory";
    public final String PROP_KEY_BROWSER_TYPE = "BrowserType";
    public final String PROP_KEY_IE_DRIVER = "webdriver.ie.driver";
    public final String PROP_KEY_IE_LOG = "webdriver.ie.driver.logfile";
    public final String PROP_KEY_IE_LOG_LEVEL = "webdriver.ie.driver.loglevel";
    public final String PROP_KEY_CHROME_DRIVER = "webdriver.chrome.driver";
    public final String PROP_KEY_CHROME_LOG = "webdriver.chrome.logfile";
    public final String PROP_KEY_EXEC_DELAY = "execution.delay";
    public final String AZURE_CONNECTION_STRING = "stringAzureConnection";
    public final String DOWNLOAD_DIRECTORY="download.default_directory";

    ConfigUtil() {
        if ( CFG_FILE_PATH.endsWith("xls") || CFG_FILE_PATH.endsWith("xlsx")) {
            configStore = new XlsConfigStore(CFG_FILE_PATH);
        } else {
            configStore = new PropFileConfigStore(CFG_FILE_PATH);
        }
    }

    public boolean getScreenShotBeforeStep() {
        String val = configStore.getProperty(PROP_KEY_SCREENSHOT_BEFORE_STEP, "false").toLowerCase().trim();
        return (val.equals("true") ? true : false);
    }
    public boolean getScreenShotAfterStep() {
        String val = configStore.getProperty(PROP_KEY_SCREENSHOT_AFTER_STEP, "false").toLowerCase().trim();
        return val.equals("true") ? true : false;
    }

    public long getExecDelay() {
        String val = configStore.getProperty(PROP_KEY_EXEC_DELAY,"0").trim();
        return Long.parseLong(val);
    }
    public String getBrowserType() {
        return configStore.getProperty(PROP_KEY_BROWSER_TYPE,"");
    }
    public String getTestSet() {
        return configStore.getProperty(PROP_KEY_TEST_SET,"");
    }
    public String getHomeURL() {
        return configStore.getProperty(PROP_KEY_HOME_URL,"");
    }
    public String getBaseURL() {
        return configStore.getProperty(PROP_KEY_BASE_URL,"");
    }

    public String getIEDriver() {
        return configStore.getProperty(PROP_KEY_IE_DRIVER, "");
    }

    public String getIELog() {
        return configStore.getProperty(PROP_KEY_IE_LOG, "");
    }

    public String getIELogLevel() {
        return configStore.getProperty(PROP_KEY_IE_LOG_LEVEL, "");
    }
    public String getChromeDriver() {
        return configStore.getProperty(PROP_KEY_CHROME_DRIVER,"");
    }
    public String getChromeLog() {
        return configStore.getProperty(PROP_KEY_CHROME_LOG,"");
    }
    public String getTestNGDir() {
        return configStore.getProperty(PROP_KEY_TESTNG_DIR,"");
    }
    public String getDBAccess() {
        return configStore.getProperty(PROP_KEY_DB_ACCESS,"");
    }
    public String getAllureResultDir() { return configStore.getProperty(PROP_KEY_ALLURE_RESULT_DIR,"allure-results");}

    //Thuynt add it to getAzureConnection
    public String getAzureConnection() {
        return configStore.getProperty(AZURE_CONNECTION_STRING, "");
    }

    public String getProperty(String key) {
        return configStore.getProperty(key,"");
    }
    public String getDirectoryDownload() {return configStore.getProperty(DOWNLOAD_DIRECTORY,"");}

}
