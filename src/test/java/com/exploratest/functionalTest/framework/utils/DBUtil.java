package com.exploratest.functionalTest.framework.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class DBUtil {

    private static final Logger logger = LoggerFactory.getLogger(DBUtil.class);

    private Connection conn = null;
    private Statement stmt = null;
    private String connStr;
    private String dbID;
    private String dbPw;
    private String driverName;

    public DBUtil(String driverName, String connStr, String dbID, String dbPw) {
        this.driverName = driverName;
        this.connStr = connStr;
        this.dbID = dbID;
        this.dbPw = dbPw;
        connectDB();
    }

    //return ture if ok, else false
    private void connectDB() {
        try {
            logger.info(String.format("driver name: %s", driverName));
            Class.forName(driverName);
            logger.debug("Connecting to database...");
            conn = DriverManager.getConnection(connStr, dbID, dbPw);
        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            logger.error("failed to connect to database", e);
        }
    }

    public boolean SelectCountMatch(String sql, String matchingNum, String compareOp) {
        logger.info(String.format("matching num: %s, compareOp: %s", matchingNum, compareOp));
        return this.SelectCountMatch(sql,Long.parseLong(matchingNum),compareOp);
    }

    // compare operator is less than, equal, bigger than, etc
    public boolean SelectCountMatch(String sql, long matchingNum, String compareOp) {
        logger.info("matching num");
        try {
            stmt = conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery(sql);
            rs.next();

            long count = rs.getLong(1);
            String op = compareOp.trim();
            if (op.trim().equals("<") && count < matchingNum) {
                return true;
            } else if (op.trim().equals("<") && count < matchingNum) {
                return true;
            } else if (op.trim().equals("<=") && count <= matchingNum) {
                return true;
            } else if (op.trim().equals("=") && count == matchingNum) {
                return true;
            } else if (op.trim().equals("==") && count == matchingNum) {
                return true;
            } else if (op.trim().equals(">") && count > matchingNum) {
                return true;
            } else if (op.trim().equals(">=") && count >= matchingNum) {
                return true;
            }
        } catch (SQLException e) {
            logger.error(String.format("failed to do select count: %s, number: %d, compare operator: %s",
                                       sql, matchingNum, compareOp),
                         e);
            logger.error(e.getMessage());
            logger.error(e.getStackTrace().toString());
            return false;
        }
        return false;
    }

    // for update, insert and delete
    public boolean ExecuteUpdate(String sql) {
        try {
            stmt = conn.createStatement();
            logger.info(String.format("b4 exec update, sql: %s", sql));
            stmt.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error(String.format("failed to run SQL: %s", sql), e);
            return false;
        }
    }

    public void CloseConnection() {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error("failed to close database connection: ", e);
            e.printStackTrace();
        }
    }

    public String SelectString(String sql) {
        String returnVal = "";
        try {
            stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery(sql);
            rs.next();
            returnVal = rs.getString(1);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(e.getStackTrace().toString());
            return "";
        }
        return returnVal;
    }
}
