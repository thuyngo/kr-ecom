package com.exploratest.functionalTest.framework.utils;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//System utilities
public class SysUtil {
    private static final String TASKLIST_WINDOWS = "tasklist";
    private static final String TASKLIST_UNIX_LIKE_OS = "ps ax";
    private static final String KILL_WINDOWS = "taskkill /IM {process} /F /T ";
    private static final String KILL_UNIX_LIKE_OS = "kill -9 {pid}";
    private static final int MAX_RETRY = 3;
    private static Logger logger = LoggerFactory.getLogger(SysUtil.class);

    private static boolean isProcessRunningInWindows(String serviceName) throws Exception {
        Process p = Runtime.getRuntime().exec(TASKLIST_WINDOWS);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                p.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            logger.debug(line);
            if (line.contains(serviceName)) {
                return true;
            }
        }

        return false;
    }

    private static long getPidOfProcessInUnixLikeOS(String serviceName) throws Exception {
        Process p = Runtime.getRuntime().exec(TASKLIST_UNIX_LIKE_OS);
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            logger.debug(line);
            if (line.contains(serviceName)) {
                try (java.util.Scanner scanner = new java.util.Scanner(line)) {
                    return scanner.nextLong();
                }
            }
        }

        return -1;
    }

    private static boolean isProcessRunningInUnixLikeOS(String serviceName) throws Exception {
        return getPidOfProcessInUnixLikeOS(serviceName) != -1;
    }

    public static boolean isProcessRunning(String serviceName) throws Exception {
        if (SystemUtils.IS_OS_WINDOWS) {
            return isProcessRunningInWindows(serviceName);
        } else {
            return isProcessRunningInUnixLikeOS(serviceName);
        }
    }

    private static void killProcessInWindows(String serviceName) throws Exception {
        int retry = 0;

        Runtime.getRuntime().exec(KILL_WINDOWS.replace("{process}", serviceName));
        retry++;
        while (isProcessRunning(serviceName) && retry < MAX_RETRY) {
            retry++;
            logger.info(String.format("retry to kill service: %s, retry count: %d", serviceName, retry));
            Thread.sleep(1000);
            Runtime.getRuntime().exec(KILL_WINDOWS.replace("{process}", serviceName));
        }

        if (retry >= MAX_RETRY) {
            logger.error(String.format("failed to kill process: %s", serviceName));
            throw new RuntimeException(String.format("failed to kill process: %s", serviceName));
        }
    }

    private static void killProcessInUnixLikeOS(String serviceName) throws Exception {
        int retry = 0;

        long pid = getPidOfProcessInUnixLikeOS(serviceName);
        if (pid != -1) {
            Runtime.getRuntime().exec(KILL_UNIX_LIKE_OS.replace("{pid}", Long.toString(pid)));
            retry++;
            while (((pid = getPidOfProcessInUnixLikeOS(serviceName)) != -1) && retry < MAX_RETRY) {
                retry++;
                logger.info(String.format("retry to kill service: %s, retry count: %d", serviceName, retry));
                Thread.sleep(1000);
                Runtime.getRuntime().exec(KILL_UNIX_LIKE_OS.replace("{pid}", Long.toString(pid)));
            }

            if (retry >= MAX_RETRY) {
                logger.error(String.format("failed to kill process: %s", serviceName));
                throw new RuntimeException(String.format("failed to kill process: %s", serviceName));
            }
        }
    }

    public static void killProcess(String serviceName) throws Exception {
        if (SystemUtils.IS_OS_WINDOWS) {
            killProcessInWindows(serviceName);
        } else {
            killProcessInUnixLikeOS(serviceName);
        }
    }
}
