package com.exploratest.functionalTest.framework.utils;

import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class ExcelReader {
    private String path;
    private FileInputStream fis = null;
    private FileOutputStream fileOut =null;
    private XSSFWorkbook workbook = null;
    private XSSFSheet sheet = null;
    private XSSFRow row   =null;
    private XSSFCell cell = null;
    protected static Logger logger = LoggerFactory.getLogger(ExcelReader.class);

    public ExcelReader(String path) {

        this.path=path;
        try {
            fis = new FileInputStream(this.path);
            workbook = new XSSFWorkbook(fis);
            sheet = workbook.getSheetAt(0);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getRowCount(int index){
        sheet = workbook.getSheetAt(index);
        int number=sheet.getLastRowNum()+1;
        return number;
    }

    public String getCellData(int sheetIndex,int colNum,int rowNum){
        try {
            sheet = workbook.getSheetAt(sheetIndex);
            row = sheet.getRow(rowNum);
            if (row != null && row.getCell(colNum).getCellType()== CellType.BOOLEAN)
                return  row.getCell(colNum).getBooleanCellValue() ? "true" : "false";
            if (row != null && row.getCell(colNum).getCellType()== CellType.NUMERIC)
                return row.getCell(colNum).getRawValue();
            return (row == null) ? null  : row.getCell(colNum).getStringCellValue();
        } catch(Exception e){
            e.printStackTrace();
            return "failed to get cell data at row: " + rowNum + ", col; " + colNum;
        }
    }

    // returns the data from a cell
    public String getCellData(String sheetName,String colName,int rowNum){
        try{
            int index = workbook.getSheetIndex(sheetName);
            int col_Num=-1;
            if(index==-1)
                return "";

            sheet = workbook.getSheetAt(index);
            row=sheet.getRow(0);
            for(int i=0;i<row.getLastCellNum();i++){
                if(row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
                    col_Num=i;
            }
            if(col_Num==-1)
                return "";

            sheet = workbook.getSheetAt(index);
            row = sheet.getRow(rowNum-1);
            if(row==null)
                return "";
            cell = row.getCell(col_Num);

            if(cell==null)
                return "";
            //System.out.println(cell.getCellType());
            if(cell.getCellType()== CellType.STRING)
                return cell.getStringCellValue();
            else if(cell.getCellType()==CellType.NUMERIC || cell.getCellType()==CellType.FORMULA ){

                String cellText  = String.valueOf(cell.getNumericCellValue());
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    // format in form of M/D/YY
                    double d = cell.getNumericCellValue();

                    Calendar cal =Calendar.getInstance();
                    cal.setTime(HSSFDateUtil.getJavaDate(d));
                    cellText =
                            (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
                    cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" +
                            cal.get(Calendar.MONTH)+1 + "/" +
                            cellText;

                    //System.out.println(cellText);

                }

                return cellText;
            }else if(cell.getCellType()==CellType.BLANK)
                return "";
            else
                return String.valueOf(cell.getBooleanCellValue());

        }
        catch(Exception e){

            e.printStackTrace();
            return "row "+rowNum+" or column "+colName +" does not exist in xls";
        }
    }

    // returns the data from a cell
    public String getCellData(String sheetName,int colNum,int rowNum){
        try{
            int index = workbook.getSheetIndex(sheetName);
            if(index==-1)
                return "";

            sheet = workbook.getSheetAt(index);
            row = sheet.getRow(rowNum-1);
            if(row==null)
                return "";
            cell = row.getCell(colNum);
            if(cell==null)
                return "";

            if(cell.getCellType()==CellType.STRING)
                return cell.getStringCellValue();
            else if(cell.getCellType()==CellType.NUMERIC || cell.getCellType()==CellType.FORMULA ){

                String cellText  = String.valueOf(cell.getNumericCellValue());
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    // format in form of M/D/YY
                    double d = cell.getNumericCellValue();

                    Calendar cal =Calendar.getInstance();
                    cal.setTime(HSSFDateUtil.getJavaDate(d));
                    cellText =
                            (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
                    cellText = cal.get(Calendar.MONTH)+1 + "/" +
                            cal.get(Calendar.DAY_OF_MONTH) + "/" +
                            cellText;
                }
                return cellText;
            }else if(cell.getCellType()==CellType.BLANK)
                return "";
            else
                return String.valueOf(cell.getBooleanCellValue());
        }
        catch(Exception e){

            e.printStackTrace();
            return "row "+rowNum+" or column "+colNum +" does not exist  in xls";
        }
    }

    public boolean setCellData(String sheetName,String colName,int rowNum, String data){
        int index = workbook.getSheetIndex(sheetName);
        if(index==-1)
            return false;
        return true;
    }

    // returns true if data is set successfully else false
    public boolean setCellData(int index,String colName,int rowNum, String data){
        try{
            fis = new FileInputStream(path);
            workbook = new XSSFWorkbook(fis);

            if(rowNum<=0)
                return false;

            int colNum=-1;


            sheet = workbook.getSheetAt(index);


            row=sheet.getRow(0);
            for(int i=0;i<row.getLastCellNum();i++){
                //System.out.println(row.getCell(i).getStringCellValue().trim());
                if(row.getCell(i).getStringCellValue().trim().equals(colName))
                    colNum=i;
            }
            if(colNum==-1)
                return false;

            sheet.autoSizeColumn(colNum);
            row = sheet.getRow(rowNum-1);
            if (row == null)
                row = sheet.createRow(rowNum-1);

            cell = row.getCell(colNum);
            if (cell == null)
                cell = row.createCell(colNum);

            // cell style
            //CellStyle cs = workbook.createCellStyle();
            //cs.setWrapText(true);
            //cell.setCellStyle(cs);
            cell.setCellValue(data);

            fileOut = new FileOutputStream(path);

            workbook.write(fileOut);

            fileOut.close();

        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     *
     * @param sheetName
     * @param colName
     * @param rowNum
     * @param data
     * @param url
     * @return returns true if data is set successfully else false
     */
    public boolean setCellData(String sheetName,String colName,int rowNum, String data,String url){
        //System.out.println("setCellData setCellData******************");
        try{
            fis = new FileInputStream(path);
            workbook = new XSSFWorkbook(fis);

            if(rowNum<=0)
                return false;

            int index = workbook.getSheetIndex(sheetName);
            int colNum=-1;
            if(index==-1)
                return false;


            sheet = workbook.getSheetAt(index);
            //System.out.println("A");
            row=sheet.getRow(0);
            for(int i=0;i<row.getLastCellNum();i++){
                //System.out.println(row.getCell(i).getStringCellValue().trim());
                if(row.getCell(i).getStringCellValue().trim().equalsIgnoreCase(colName))
                    colNum=i;
            }

            if(colNum==-1)
                return false;
            sheet.autoSizeColumn(colNum);
            row = sheet.getRow(rowNum-1);
            if (row == null)
                row = sheet.createRow(rowNum-1);

            cell = row.getCell(colNum);
            if (cell == null)
                cell = row.createCell(colNum);

            cell.setCellValue(data);
            XSSFCreationHelper createHelper = workbook.getCreationHelper();

            //cell style for hyperlinks
            //by default hypelrinks are blue and underlined
            CellStyle hlink_style = workbook.createCellStyle();
            XSSFFont hlink_font = workbook.createFont();
            hlink_font.setUnderline(XSSFFont.U_SINGLE);
            hlink_font.setColor(IndexedColors.BLUE.getIndex());
            hlink_style.setFont(hlink_font);
            //hlink_style.setWrapText(true);

            XSSFHyperlink link = createHelper.createHyperlink(HyperlinkType.FILE);
            link.setAddress(url);
            cell.setHyperlink(link);
            cell.setCellStyle(hlink_style);

            fileOut = new FileOutputStream(path);
            workbook.write(fileOut);

            fileOut.close();

        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // find whether sheets exists
    public boolean isSheetExist(String sheetName){
        int index = workbook.getSheetIndex(sheetName);
        if(index==-1){
            index=workbook.getSheetIndex(sheetName.toUpperCase());
            return index != -1;
        }
        else
            return true;
    }

    // returns number of columns in a sheet
    public int getColumnCount(String sheetName){
        // check if sheet exists
        if(!isSheetExist(sheetName))
            return -1;

        sheet = workbook.getSheet(sheetName);
        row = sheet.getRow(0);

        if(row==null)
            return -1;

        return row.getLastCellNum();
    }


    /**
     * Return rows that match certain value, case insensitive, ignore leading, trailing space
     */
    public Iterator<Object []> getRows(String sheetName, String match, int colNum) {
        List<Object []> filteredRows = new ArrayList<Object []>();
        sheet = workbook.getSheet(sheetName);
        logger.debug("sheet: " + sheetName + " ,colnum: " + colNum + "rows: " + sheet.getLastRowNum());
        for (Row row : sheet) {
            logger.debug("sheet: " + sheetName + ", row: " + row.getRowNum());
            if (row.getCell(colNum).toString().trim().toLowerCase().equals(match.trim().toLowerCase())) {
                List<String> matchingRow = new ArrayList<String>();
                for (Cell cell : row) {
                    matchingRow.add(cell.toString().trim());
                }
                Object [] obj = new Object [1];
                obj[0] = matchingRow;
                filteredRows.add(obj);
                logger.debug("Sheet: " + sheetName + ", matching row" + matchingRow.toString());
            }
        }
        return filteredRows.iterator();
    }

    // to run this on stand alone
    public static void main(String arg[]) {
        ExcelReader datatable = null;

        datatable = new ExcelReader("C:\\temp\\testset\\autoApproach.xlsx");
        for(int col=0 ;col< datatable.getColumnCount("Test Cases"); col++){
            logger.info(datatable.getCellData("Test Cases", col, 2));
            if (datatable.getCellData("Test Cases", col, 2).trim().toUpperCase() == "Y") {

            }
        }
    }
}
