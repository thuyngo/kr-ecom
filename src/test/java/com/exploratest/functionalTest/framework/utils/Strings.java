package com.exploratest.functionalTest.framework.utils;

// https://stackoverflow.com/questions/26794275/how-do-i-ignore-case-when-using-startswith-and-endswith-in-java/38947571#38947571
public final class Strings {
    public static boolean startsWithIgnoreCase(String string, String prefix) {
        return string.regionMatches(true, 0, prefix, 0, prefix.length());
    }

    public static boolean endsWithIgnoreCase(String string, String suffix) {
        int suffixLength = suffix.length();
        return string.regionMatches(true, string.length() - suffixLength, suffix, 0 , suffixLength);
    }
}
