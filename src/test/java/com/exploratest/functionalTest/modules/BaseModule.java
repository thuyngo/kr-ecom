package com.exploratest.functionalTest.modules;

import com.exploratest.functionalTest.framework.core.TestEngineListener;
import com.exploratest.functionalTest.framework.utils.ConfigUtil;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Listeners({ TestEngineListener.class })
public abstract class BaseModule {
    private static final int DEFAULT_WAIT_SECONDS = 10;
    private final String CUR_PACKAGE_NAME = String.format("%s.", this.getClass().getPackage().getName());

    protected WebDriver webDriver;
    protected JavascriptExecutor js;
    protected String homeURL;
    protected long startTime, endTime;
    protected Logger logger = LoggerFactory.getLogger(this.getClass().getName());


    public BaseModule(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.js = (JavascriptExecutor)webDriver;
        this.homeURL = ConfigUtil.INSTANCE.getHomeURL();
    }

    protected WebElement click(String xpath) {
        return click(By.xpath(xpath), DEFAULT_WAIT_SECONDS);
    }

    //click for webview
    protected WebElement click(By by, int waitSeconds) {
        WebElement we = this.getElement(by, waitSeconds, true);
        if (we != null) {
            we.click();
        }
        return we;
    }

    protected WebElement clickOnVisible(String xpath, int waitSeconds) {
        return clickOnVisible(By.xpath(xpath), waitSeconds);
    }

    protected WebElement clickOnVisible(By by, int waitSeconds) {
        WebDriverWait wait = new WebDriverWait(this.webDriver, waitSeconds);
        WebElement we = wait
                .pollingEvery(waitSeconds, TimeUnit.SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
        if (we == null) {
            logger.warn(String.format("element with: %s not found", by.toString()));
            Reporter.log(String.format("element with: %s not found<br />", by.toString()));
            return null;
        }
        we.click();
        return we;
    }

    /**
     *
     * @param xpath
     * @return web element, wait 5 up secconds before giving up, which would fail the current test step
     */
    protected WebElement getElement(String xpath) {
        //default to 5 seconds
        return this.getElement(xpath, DEFAULT_WAIT_SECONDS, true);
    }

    protected WebElement getElement(String xpath, int waitSeconds, boolean failOnError) {
        //default to 5 seconds
        return this.getElement(By.xpath(xpath), waitSeconds, failOnError);
    }

    protected WebElement getElement(By by, int waitSeconds, boolean failOnError) {
        WebElement we = null;
        try {
            we = (new WebDriverWait(this.webDriver, waitSeconds))
                    .pollingEvery(1, TimeUnit.SECONDS)
                    .until(ExpectedConditions.presenceOfElementLocated(by)
                    );
        } catch (TimeoutException e) {
            logger.error(String.format("element not found: %s", by.toString()));
            if (failOnError) {
                Assert.fail(String.format("element not found: %s", by.toString()));
            }
            return null;
        }
        this.thinkTime(ConfigUtil.INSTANCE.getExecDelay());
        return we;
    }

    protected static boolean getWebElementAttributeAsBoolean(WebElement element, String attribute) {
        return Boolean.parseBoolean(element.getAttribute(attribute));
    }

    protected static boolean getWebElementAriaExpandedAttribute(WebElement element) {
        return getWebElementAttributeAsBoolean(element, "aria-expanded");
    }

    protected static boolean getWebElementAriaSelectedAttribute(WebElement element) {
        return getWebElementAttributeAsBoolean(element, "aria-selected");
    }

    protected static String getXpathContainsTargetAttribute(String targetAttribute, String value) {
        return String.format("[contains(concat(' ', normalize-space(@%s), ' '), ' %s ')]", targetAttribute.trim(), value.trim());
    }

    protected static String getXpathContainsClassAttribute(String className) {
        return getXpathContainsTargetAttribute("class", className);
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = driver -> (this.js.executeScript("return document.readyState;").equals("complete"));

        WebDriverWait wait = new WebDriverWait(this.webDriver, 30);
        try {
            wait.until(expectation);
        } catch (WebDriverException e) {
            Assert.assertFalse(true, "Timeout waiting for Page Load Request to complete.");
        }
    }

    protected WebElement getElement(By by, int waitSeconds) {
        return this.getElement(by, waitSeconds, true);
    }

    protected WebElement getNthElement(String xpath, int elementIndex, int waitSeconds, boolean failOnError) {
        WebElement we = null;
        try {
            List<WebElement> elements = (new WebDriverWait(this.webDriver, waitSeconds))
                    .pollingEvery(1, TimeUnit.SECONDS)
                    .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            int count = elements.size();
            if (count < elementIndex || elementIndex <= -1) {
                elementIndex = count - 1;  //get last element
            }

            we = elements.get(elementIndex);
            this.thinkTime(ConfigUtil.INSTANCE.getExecDelay());
        } catch (TimeoutException e) {
            logger.error(String.format("xpath not found: %s", xpath));
            if (failOnError) {
                Assert.fail(String.format("xpath not found: %s", xpath));
            }
            return null;
        }
        return we;
    }

    protected WebElement getNthElement(String xpath, int elementIndex) {
        return this.getNthElement(xpath, elementIndex, 5, true);
    }

    protected WebElement getFirstElement(String xpath) {
        return this.getNthElement(xpath, 0, 5, true);
    }

    protected WebElement getLastElement(String xpath) {
        return this.getNthElement(xpath, -1, 5, true);
    }

    private String concatPackageNameAndKey(String key) {
        return String.format("%s%s", CUR_PACKAGE_NAME, key.trim());
    }

    protected void saveVar(String key, String value) {
        System.setProperty(concatPackageNameAndKey(key), value);
    }

    protected String getVar(String key) {
        return System.getProperty(concatPackageNameAndKey(key));
    }

    protected String clearVar(String key) {
        return System.clearProperty(concatPackageNameAndKey(key));
    }

//    public void CloseAllBrowsers() {
//        try{
//            webDriver.close();
//            webDriver.quit();
//            logger.info("Browser closed");
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//    }

    public void VerifyTextExists(String xpath, String expectedText) {
        String getText = this.webDriver.findElement(By.xpath(xpath)).getText();

        if (getText.equals(expectedText)) {
            logger.info(String.format("Expected Text : %s Matched the Text on Screen : %s", expectedText, getText));
        } else {
            String errorMessage = String.format("FAILED: Expected Text : %s DOES NOT Match the Text on Screen : %s", expectedText, getText);
            logger.error(errorMessage);
            Assert.fail(errorMessage);
        }
    }

    protected void thinkTime(long milliseconds) {
        if (milliseconds <= 0) {
            return;
        }

        try {
            this.startTime();
            Thread.sleep(milliseconds);
        } catch (InterruptedException ex) {
            logger.error(String.format("think time interrupted, expected duration: %d", milliseconds), ex);
            this.endTime();
            this.difference("think time interrupted");
            Assert.fail("think time interrupted", ex);
        }
    }

    //**** Measure Start Time
    protected void startTime() {
        this.startTime = System.currentTimeMillis();
        //SimpleDateFormat startdf = new SimpleDateFormat("HH:MM:ss:SSS");
        //logger.info("Measurement, startTime, "+ action + "," + startdf.format(startTime));
    }

    //*** Measure End Time
    protected void endTime() {
        this.endTime = System.currentTimeMillis();
    }

    //**** Measure Difference
    protected void difference(String action) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss:SSS");
        SimpleDateFormat difference = new SimpleDateFormat("ss:SSS");
        String sStartTime = df.format(this.startTime);
        String sEndTime = df.format(this.endTime);
        long timeDiff = this.endTime - this.startTime;
        String sTimeDiff = difference.format(timeDiff);
        logger.info(String.format("Time Measurement, %s,%s,%s,%s", action, sStartTime, sEndTime, sTimeDiff));
        //Date formatEndDate = new Date(timeEnd);
        //Date formatStartDate = new Date(timeStart);
        //return differenceTime;
    }

    /**
     *Created by Thuynt
     * @param by
     * @return
     */
    public boolean isElementPresent(By by) {
        boolean isPresent = true;
        //waitForPageLoaded();
        //search for elements and check if list is empty
        if (this.webDriver.findElements(by).isEmpty()) {
            isPresent = false;
        }
        //rise back implicitly wait time
        //this.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
        return isPresent;
    }

    protected void scrollElementInToView(WebElement element) {
        this.js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    protected boolean isAttribtuePresent(WebElement element, String attribute) {
        Boolean result = false;
        try {
            String value = element.getAttribute(attribute);
            if (value != null){
                result = true;
            }
        } catch (Exception e) {}

        return result;
    }
}
