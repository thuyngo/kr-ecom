package com.exploratest.functionalTest.modules.EILotteCancel;

import com.exploratest.functionalTest.modules.RLCommon;
import com.exploratest.functionalTest.modules.System.ModuleMenu;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.*;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EILotteCancel extends RLCommon {

    public EILotteCancel(WebDriver webDriver, ModuleMenu moduleMenu) {
        super(webDriver, moduleMenu);
    }

    public static void eiLotteCancel(String[] args)throws InterruptedException, AWTException, IOException {

        String usernameString = "20@3945";
        String passwordString = "!20@3945";

        System.setProperty("webdriver.ie.driver", "C:\\SeleniumDrivers\\IEDriverServer.exe");
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        capabilities.setCapability("requireWindowFocus", true);
        capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        WebDriver webDriver = new InternetExplorerDriver(capabilities);
        //webDriver.get("https://translate.google.com/?hl=zh-TW#view=saved");
        webDriver.get("https://logismart.lotte.com/IL5/Login.aspx");

        Actions actions = new Actions(webDriver);

        /*
        input("//input[@name='plLogin$tbID']", webDriver, usernameString);
        pause(500);
        input("//input[@name='plLogin$tbPW']", webDriver, passwordString);
        pause(500);
        click("//input[@name='plLogin$btnLogin']", webDriver);
        pause(500);
        */
        click("//td[@id='pcContent_HCB-1']", webDriver); //cross icon
        pause(500);
        click("//img[@id='aspxMenuTab_ctl15_DXI1_Img']", webDriver); //partner
        pause(500);
        click("//img[@id='aspxMenuLeft_I1i14_Img']", webDriver); //application
        pause(15000);


        pause(2000);
        Robot robot = new Robot();
        robot.mouseMove(0,0);
        robot.mouseMove(700,120); //locate task bar
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        pause(200);
        robot.mouseMove(0,0); //drag window to top left corner
        pause(200);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        pause(1000);
        robot.mouseMove(0,0);
        robot.mouseMove(156,80);
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseMove(100,80); //highlight date
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date todate1 = cal.getTime();
        String fromdate = formatter.format(todate1);
        robotInput(fromdate); //date input
        pause(200);
        robotMouseClick(538, 80); //drop down list
        robotMouseClick(504, 136); //pick item

        //robotMouseClick(30, 40);   //refresh button which is under issue

        robotMouseClick(132, 374);
        robotMouseClick(772, 480);
        robotInput("F:\\Department_Store\\EILotte\\Cancel\\WM_EILotte_Cancel");
        //robotInput("Test");
        robotMouseClick(632, 426); //save
        robotMouseClick(812, 480); //no button
        robotMouseClick(758, 12); //close


       /* Robot robot = new Robot();
        pause(5000);
        robot.mouseMove(660,320);
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        String text = "Hello World";
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, stringSelection);

        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        */
    }


    public static void robotMouseClick(int x, int y) throws AWTException, InterruptedException{
        Robot robot = new Robot();
        robot.mouseMove(0,0);
        robot.mouseMove(x,y);
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        pause(2000);
    }

    public static void robotInput(String input) throws java.awt.AWTException{
        Robot robot = new Robot();
        String text = input;
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, stringSelection);

        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
    }

    public static void pause(int i)throws InterruptedException{
        try{
            Thread.sleep(i);
        } catch(WebDriverException e){}
    }

    public static void locate(String xpath, WebDriver webDriver){
        Actions actions = new Actions(webDriver);
        By by = By.xpath(xpath);
        WebElement we = (new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.presenceOfElementLocated(by)
                ));
        actions.moveToElement(we);
    }

    public static void click(String xpath, WebDriver webDriver){
        Actions actions = new Actions(webDriver);
        By by = By.xpath(xpath);
        WebElement we = (new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.presenceOfElementLocated(by)
                ));
        actions.moveToElement(we).click().perform();
    }

    public static void input(String xpath, WebDriver webDriver, String input){
        Actions actions = new Actions(webDriver);
        By by = By.xpath(xpath);
        WebElement we = (new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.presenceOfElementLocated(by)
                ));
        actions.moveToElement(we).click().perform();
        we.sendKeys(input, Keys.TAB);
    }

}
