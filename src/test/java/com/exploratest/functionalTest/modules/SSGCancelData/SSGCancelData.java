package com.exploratest.functionalTest.modules.SSGCancelData;

import com.exploratest.functionalTest.modules.RLCommon;
import com.exploratest.functionalTest.modules.System.ModuleMenu;
import org.openqa.selenium.*;
import org.testng.Assert;

public class SSGCancelData extends RLCommon {
    public SSGCancelData(WebDriver webDriver, ModuleMenu moduleMenu) {
        super(webDriver, moduleMenu);
    }

    public void DownloadCancelData() {
        try {
            this.webDriver.navigate().to(homeURL);
            this.thinkTime(10000);
            WebElement favoriteButton = this.getElement("//button[@id='bookmarkBtn']");
            favoriteButton.click();
            this.thinkTime(10000);
            WebElement firstMenu = this.getElement("//li[@id='lnbM_5000000982']//div//a");
            firstMenu.click();
            this.thinkTime(10000);
            webDriver.switchTo().frame(webDriver.findElement(By.xpath("//iframe[contains(@id, 'iframe_50')]")));
            this.thinkTime(10000);
            WebElement posSelectBox = this.getElement("//select[contains(@title, 'POS')][contains(@id, 'posSendYn')]");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", posSelectBox);
            posSelectBox.click();
            this.thinkTime(10000);
            this.getElement("//select[contains(@title, 'POS')][contains(@id, 'posSendYn')]//option[1]").click();
            this.thinkTime(10000);
            WebElement checkbox1 = this.getElement("//tbody/tr[5]/td[2]/label[1]/input[1]");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", checkbox1);
            if(this.isAttribtuePresent(checkbox1, "checked")){
                checkbox1.click();
            }
            this.thinkTime(10000);
            WebElement checkbox2 = this.getElement("//tbody/tr[5]/td[2]/label[2]/input[1]");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", checkbox2);
            checkbox2.click();
            this.thinkTime(10000);
            /*WebElement checkbox3 = this.getElement("//tr[6]//td[2]//label[1]//input");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", checkbox3);
            if(!this.isAttribtuePresent(checkbox3, "checked")){
                checkbox3.click();
            }
            this.thinkTime(10000);
            WebElement checkbox4 = this.getElement("//tr[6]//td[2]//label[2]//input");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", checkbox4);
            checkbox4.click();
            this.thinkTime(10000);*/
            WebElement searchButton = this.getElement("//button[contains(@class, 'btn_point_ico')][contains(@id, 'searchBtn')]");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", searchButton);
            searchButton.click();
            this.thinkTime(30000);
            WebElement selectAll = this.getElement("//div[contains(@class, 'hdrcell')]//input[@type='checkbox']");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", selectAll);
            selectAll.click();
            this.thinkTime(20000);
            WebElement downloadByExcel = this.getFirstElement("//a[contains(@class, 'ico_excel')]");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", downloadByExcel);
            downloadByExcel.click();
            this.thinkTime(20000);
        }catch (WebDriverException e) {
            logger.error("Failed to download Cancel data file");
            Assert.fail("Failed to download file");
        }
    }
}
