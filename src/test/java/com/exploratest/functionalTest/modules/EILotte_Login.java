package com.exploratest.functionalTest.modules;

import com.exploratest.functionalTest.modules.System.ModuleMenu;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EILotte_Login extends RLCommon {
    public EILotte_Login(WebDriver webDriver, ModuleMenu moduleMenu) {
        super(webDriver, moduleMenu);
    }

    public void EILotteLogin(String userName, String password) {
        logger.info(String.format("TEST CASE STARTED - Login, homeURL: %s", homeURL));
        this.webDriver.navigate().to(homeURL);
        this.thinkTime(8000);
        String url = this.webDriver.getCurrentUrl();
        WebElement userIdInput = this.getElement("//input[contains(@name, 'plLogin')][contains(@id, 'tbID')]");
        userIdInput.click();
        userIdInput.clear();
        userIdInput.sendKeys(userName);
        this.thinkTime(5000);
        WebElement passWordInput = this.getElement("//input[contains(@name, 'plLogin')][contains(@id, 'tbPW')]");
        passWordInput.click();
        passWordInput.clear();
        passWordInput.sendKeys(password);
        this.thinkTime(3000);
        WebElement loginButton = this.getElement("//input[@type='submit'][contains(@id, 'btnLogin')]");
        loginButton.click();
        this.thinkTime(15000);
        this.getElement("//label[@id='lbUser']", 60, true);
    }
    public void EILotteLogout() {
        logger.debug("before logout");
        this.waitForPageLoaded();
        this.thinkTime(9000);
        WebElement logoutButton = this.getElement("//td[@class='utInfo']//a");
        ((JavascriptExecutor)
                webDriver).executeScript ("arguments[0].scrollIntoView();", logoutButton);
        logoutButton.click();
        this.thinkTime(5000);
        WebElement logoutConfirm = (new WebDriverWait(webDriver, 40))
                .until( ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@id, 'pcLogout_btnLogout_CD')]//span")));
        logoutConfirm.click();
        this.thinkTime(9000);
        WebElement messageIcon = (new WebDriverWait(webDriver, 40))
                .until( ExpectedConditions.presenceOfElementLocated(By.xpath("//input[contains(@name, 'plLogin')][contains(@id, 'tbID')]")));
        logger.debug("Logout successfully");
    }
}
