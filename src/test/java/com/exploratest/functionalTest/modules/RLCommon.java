package com.exploratest.functionalTest.modules;

import com.exploratest.functionalTest.framework.utils.ConfigUtil;
import com.exploratest.functionalTest.framework.utils.Strings;
import com.exploratest.functionalTest.modules.System.ModuleMenu;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.io.File;
import java.io.FileFilter;
import java.io.InputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.InvalidKeyException;
import java.util.Arrays;

public class RLCommon extends BaseModule {
    protected ModuleMenu moduleMenu;
    //protected Toolbar toolbar;

    public RLCommon(WebDriver webDriver, ModuleMenu moduleMenu) {
        super(webDriver);
        this.moduleMenu = moduleMenu;
        //this.toolbar = toolbar;
    }

    public void InputFilter(String filterStr) {
        WebElement we = this.getFirstElement("//input[@name='QuickFilterControl_Input']");
        we.click();
        we.sendKeys(filterStr);
        this.thinkTime(500);
    }



    protected void inputTextWithXpath(String xpath, String inputText, boolean needConfirm) {
        if (inputText == null || inputText.trim().isEmpty()) {
            return;
        }

        WebElement we = this.getElement(xpath);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(we).click().perform();

        if (needConfirm) {
            // TODO: is the below statement required??
            actions.moveToElement(we).click().perform();
            we.sendKeys(inputText, Keys.chord(Keys.ALT, Keys.ENTER));
            //textbox.sendKeys(Keys.chord(Keys.ALT, Keys.ENTER));
        } else {
            we.sendKeys(inputText, Keys.TAB);
        }
    }

    /**
     * for form filling text
     * @param basePath
     * @param label
     * @param inputText
     * @param needConfirm
     */
    protected void inputTextInTextbox(String basePath, String label, String inputText, boolean needConfirm) {
        if (inputText == null || inputText.trim().isEmpty()) {
            return;
        }

        String xpath = String.format("%s//input[@role='textbox'][contains(@aria-label,'%s')]", basePath, label);
        try {
            inputTextWithXpath(xpath, inputText, needConfirm);
        } catch (WebDriverException e) {
            logger.error(String.format("Web driver error for input text: %s, label: %s", inputText, label), e);
            Assert.fail(String.format("inputTextInTextbox failed, xpath: %s", xpath), e);
        }
    }

    /**
     * Thuynt add this function to create new product service
     * @param basePath
     * @param label
     * @param inputText
     * @param needConfirm
     */
    protected void inputTextInInput(String basePath, String label, String inputText, boolean needConfirm) {
        if (inputText == null || inputText.trim().isEmpty()) {
            return;
        }

        String xpath = String.format("%s//input[@type='text'][contains(@aria-label,'%s')]", basePath, label);
        try {
            inputTextWithXpath(xpath, inputText, needConfirm);
        } catch (WebDriverException e) {
            logger.error(String.format("Web driver error for input text: %s, label: %s", inputText, label), e);
            Assert.fail(String.format("inputTextInInput failed, xpath: %s", xpath), e);
        }
    }

    /**
     * Thuynt add to create new Service Item
     * @param basePath
     * @param label
     * @param inputText
     */
    protected void inputTextInInput(String basePath, String label, String inputText) {
        this.inputTextInInput(basePath, label, inputText, false);
    }

    protected void inputTextInTextbox(String basePath, String label, String inputText) {
        this.inputTextInTextbox(basePath, label, inputText, false);
    }

    protected void inputTextWithXpath(String xpath, String inputText) {
        this.inputTextWithXpath( xpath, inputText, false );
    }

    //protected void selectCombo(WebElement we, String xpath, String selection) {
    //    we.findElement(By.xpath(xpath)).click();
    //}

    protected void toggleYesNo(String basePath, String label, String yesNo) {
        String xpath = String.format("%s//span[@class='toggle-box'][@aria-label='%s']", basePath, label);
        try {
            this.getElement(xpath,5,true).click();
        } catch (WebDriverException e) {
            logger.error(String.format("failed to toggle, xpath: %s", xpath), e);
            Assert.fail(String.format("toogleYesNo failed, xpath: %s", xpath), e);
        }
    }

    public static void clickElementIfNotExpanded(WebElement element) {
        if (element != null && !getWebElementAriaExpandedAttribute(element)) {
            element.click();
        }
    }

    public void UpdateFileName(String filePath, String ext, String newPathFileName) {

        File newfile = this.getTheNewestFile(filePath, ext);
        File existedFile = new File(newPathFileName);
        logger.error("FileTesttttttt:" + existedFile.toString());
        if(existedFile.exists()) {
            existedFile.delete();
            this.thinkTime(3000);
            newfile.renameTo(existedFile);
            //newfile.delete();
            this.thinkTime(3000);

        }
        else {
            newfile.renameTo(new File(newPathFileName));
        }

        this.thinkTime(5000);
    }

    public File getTheNewestFile(String filePath, String ext) {
        File theNewestFile = null;
        File dir = new File(filePath);
        FileFilter fileFilter = new WildcardFileFilter("*." + ext);
        File[] files = dir.listFiles(fileFilter);

        if (files.length > 0) {
            /** The newest file comes first **/
            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
            theNewestFile = files[0];
        }

        return theNewestFile;
    }
    public static void deleteColumn(String filePath){
        //String fileName = "/Users/tuongle/Downloads/Test.xls";
        File file = new File(filePath);
        Document doc = null;
        try {
            doc = Jsoup.parse(file, "utf-8");
            Elements listTrTags = doc.select("tr");
            Elements listTdTags;
            int numberOfShellRemoved = 0;
            for (Element element : listTrTags){
                listTdTags = element.select("td");
                for (int column = 0; column < listTdTags.size(); column++) {
                    if (column == 5 || column == 14 || column == 35) {
                        System.out.println("Column value to be removed: "+ listTdTags.get(column));
                        listTdTags.get(column).remove();
                        numberOfShellRemoved++;
                    }
                }
            }
            doc.select("tr");
            FileUtils.writeStringToFile(file, doc.toString(),"utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Element divTag = doc.getElementById("mydiv");

    }
   public void DeleteAllFiles(String filepath){
        try{
            File dir = new File(filepath);
            for (File file: dir.listFiles()) {
                if (!file.isDirectory())
                    file.delete();
            }
        }catch (Exception e){
            logger.error("Cannot delete all files in this folder.");
        }

    }

    public void DeleteFiles(String filePath, String ext){

        try{
            File newFile = this.getTheNewestFile(filePath, ext);
            if (newFile.exists()) {
                //logger.error("TESSTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
                newFile.delete();
            } else {
                return;
            }
        }catch (Exception e){
            logger.error("Cannot delete the files in this folder");
        }
    }
    public boolean isAttribtuePresent(WebElement element, String attribute) {
        Boolean result = false;
        try {
            String value = element.getAttribute(attribute);
            if (value != null){
                result = true;
            }
        } catch (Exception e) {}

        return result;
    }

}
