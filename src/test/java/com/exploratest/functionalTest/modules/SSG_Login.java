package com.exploratest.functionalTest.modules;

import com.exploratest.functionalTest.framework.utils.ConfigUtil;
import com.exploratest.functionalTest.framework.utils.Strings;
import com.exploratest.functionalTest.modules.System.ModuleMenu;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SSG_Login extends RLCommon {
    public SSG_Login(WebDriver webDriver, ModuleMenu moduleMenu) {
        super(webDriver, moduleMenu);
    }

    //Microsoft online login
    public void SSGReturnLogin(String userId, String password) {
        logger.info(String.format("TEST CASE STARTED - Login, homeURL: %s", homeURL));
        this.webDriver.navigate().to(homeURL);
        webDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        //logger.info("THUYNTTTTTTTTTTTTTTTTTTTT" + webDriver.getCurrentUrl());
        this.thinkTime(10000);
        //this.webDriver.navigate().refresh();

        //String url = this.webDriver.getCurrentUrl();
        WebElement messageIcon = (new WebDriverWait(webDriver, 300))
                .until( ExpectedConditions.presenceOfElementLocated( By.xpath( "//a[contains(@class, 'btn_login')]" )));

        WebElement userIdInput = this.getElement("//input[contains(@id, 'userId')]");
        WebDriverWait wait1 = new WebDriverWait(webDriver, 300);
        wait1.until(ExpectedConditions.elementToBeClickable(userIdInput));
        userIdInput.click();
        this.thinkTime(5000);
        userIdInput.clear();
        userIdInput.sendKeys(userId);
        this.thinkTime(7000);
        WebElement passWordInput = this.getElement("//input[contains(@type, 'password')][contains(@id, 'userPwd')]");
        passWordInput.click();
        this.thinkTime(5000);
        passWordInput.clear();
        passWordInput.sendKeys(password);
        this.thinkTime(7000);
        WebElement loginButton = this.getElement("//a[contains(@class, 'btn_login')]");
        loginButton.click();
        this.thinkTime(15000);
        //this.getElement("//h1[contains(text(), 'Back Office')]", 100, true);
        //Assert.assertTrue(this.isElementPresent(By.xpath()));
        WebDriverWait wait = new WebDriverWait(this.webDriver, 100);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(), 'Office')]")));
        Assert.assertTrue(this.isElementPresent(By.xpath("//a[contains(text(), 'Office')]")), "Failed to login into system");
        this.thinkTime(10000);
        //thinkTime(500);
    }

    public void Logout() {
        logger.debug("before logout");
        this.waitForPageLoaded();
        this.webDriver.navigate().to(homeURL);
        this.waitForPageLoaded();
        this.getElement("//a[@class='user_select userSelect']").click();
        this.thinkTime(15000);
        this.getElement("//div[@class='user_info_unfold userInfoUnfold']//a[@id='logOut']").click();
        this.thinkTime(15000);
        // if (ConfigUtil.INSTANCE.getBrowserType().equals(BrowserType.CHROME)) {
        //     this.getElement("//title[text()='Sign in to your account']", 20, true);
        // }
        //String url = this.webDriver.getCurrentUrl();
        //boolean correctUrl = Strings.startsWithIgnoreCase(url, "https://login.microsoftonline.com/RalphLauren.com") || Strings.startsWithIgnoreCase(url, "https://login.microsoftonline.com/Asia.PoloRalphLauren.com");
        //Assert.assertTrue(correctUrl);
        //logger.debug("logged out");
    }

}
