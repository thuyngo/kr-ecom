package com.exploratest.functionalTest.modules.SSGOrder;

import com.exploratest.functionalTest.modules.RLCommon;
import com.exploratest.functionalTest.modules.System.ModuleMenu;
import org.openqa.selenium.*;
import org.testng.Assert;

public class SSGOrder extends RLCommon {
    public SSGOrder(WebDriver webDriver, ModuleMenu moduleMenu) {super(webDriver, moduleMenu);}

    public void DownloadFile(){
        try {
            //first page
            this.webDriver.navigate().to(homeURL);
            this.thinkTime(10000);
            WebElement favoriteButton = this.getElement("//button[@id='bookmarkBtn']");
            favoriteButton.click();
            this.thinkTime(10000);
            WebElement firstMenu = this.getElement("//li[@id='lnbM_5000000982']//div//a");
            firstMenu.click();
            this.thinkTime(10000);
           /* webDriver.switchTo().frame(webDriver.findElement(By.xpath("//iframe[contains(@id, 'iframe_5')]")));
            this.thinkTime(10000);
            WebElement searchButton = this.getElement("//button[contains(@class, 'btn_point_ico')][contains(@id, 'searchBtn')]");
            ((JavascriptExecutor)
                    webDriver).executeScript("arguments[0].scrollIntoView();", searchButton);
            searchButton.click();
            this.thinkTime(20000);
            WebElement selectAllButton = this.getElement("//div[@class='hdrcell']//input");
            selectAllButton.click();
            this.thinkTime(10000);
            WebElement pickButton = this.getElement("//div[@class='btnset_top']//button[@id='pickIndiBtn']");
            pickButton.click();
            this.thinkTime(10000);
            //handle popup
            Alert alert = webDriver.switchTo().alert();
            alert.accept();
            this.thinkTime(10000);

            webDriver.switchTo().defaultContent();
            this.thinkTime(10000);
            //second page
            WebElement secondButton = this.getElement("//div[@id='aside']//p[2]//a[1]//b[1]");
            ((JavascriptExecutor)
                    webDriver).executeScript("arguments[0].scrollIntoView();", secondButton);
            secondButton.click();
            this.thinkTime(10000);*/

            WebElement lastFrameElement = this.getLastElement("//iframe[contains(@id, 'iframe_5')]");
            webDriver.switchTo().frame(lastFrameElement);
            this.thinkTime(10000);
            //logger.error("Failed to download fileTHUYNTTTTTTTTTTTTTT");


            WebElement dropdownButton = this.getElement("//div[@class='table_area mb20']//select[@id='posSendYn']");
            dropdownButton.click();
            this.thinkTime(10000);
            WebElement firstChoise = this.getElement("//div[@class='table_area mb20']//select[@id='posSendYn']//option[1]");
            firstChoise.click();
            this.thinkTime(5000);
            WebElement checkBox1 = this.getElement("//label[1]//input[contains(@id, 'shppStatCd')]");
            if (!isAttribtuePresent(checkBox1, "checked")){
                checkBox1.click();
            }
            this.thinkTime(2000);
            WebElement checkBox2 = this.getElement("//label[2]//input[contains(@id, 'shppStatCd')]");
            if(isAttribtuePresent(checkBox2, "checked")) {
                checkBox2.click();
            }

            WebElement searchButton1 = this.getElement("//button[contains(@class, 'btn_point_ico')][contains(@id, 'searchBtn')]");
            ((JavascriptExecutor)
                    webDriver).executeScript("arguments[0].scrollIntoView();", searchButton1);
            searchButton1.click();
            this.thinkTime(25000);
            WebElement selectAllButton1 = this.getElement("//div[@class='hdrcell']//input");
            selectAllButton1.click();
            this.thinkTime(10000);
            WebElement posButton = this.getElement("//div[@class='btnset_top']//button[@id='posBatchSendBtn']");
            posButton.click();
            this.thinkTime(10000);
            //handle popup
            Alert alert1 = webDriver.switchTo().alert();
            alert1.accept();
            this.thinkTime(10000);
            WebElement downloadExcel = this.getElement("//div[@class='h_area']//a[@class='ico_excel']");
            downloadExcel.click();
            this.thinkTime(25000);
        }catch (WebDriverException e){
            logger.error("Failed to download file");
            Assert.fail("Failed to download file");
        }
    }

}
