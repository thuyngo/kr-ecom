package com.exploratest.functionalTest.modules.SSGReturn;

import com.exploratest.functionalTest.modules.RLCommon;
import com.exploratest.functionalTest.modules.System.ModuleMenu;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class SSGReturn extends RLCommon {

    public SSGReturn(WebDriver webDriver, ModuleMenu moduleMenu) {
        super(webDriver, moduleMenu);
    }

    public void DownloadFile () {
        try{
            this.webDriver.navigate().to(homeURL);
            this.thinkTime(10000);
            WebElement favoriteButton = this.getElement("//button[@id='bookmarkBtn']");
            favoriteButton.click();
            this.thinkTime(10000);
            WebElement thirdManaMenu = this.getElement("//li[@id='lnbM_5000000989']//div//a");
            thirdManaMenu.click();
            this.thinkTime(10000);
            webDriver.switchTo().frame(webDriver.findElement(By.xpath("//iframe[contains(@id, 'iframe_5')]")));
            this.thinkTime(10000);
           /* WebElement checkBox1 = this.getElement("//tr[4]//td[@colspan='3']//label[1]//input");
            checkBox1.click();
            this.thinkTime(10000);
            WebElement checkBox2 = this.getElement("//tr[4]//td[@colspan='3']//label[2]//input");
            checkBox2.click();
            this.thinkTime(10000);
            WebElement checkBox3 = this.getNthElement("//label[contains(@class, 'mr16')]//input[@id='shppMainCd']", 2);
            checkBox3.click();*/

            WebElement periodChoice = this.getElement("//select[@name='choicePeriod']");
            periodChoice.click();
            this.thinkTime(5000);
            WebElement thirdChoice = this.getElement("//select[@name='choicePeriod']//option[contains(text(), '15')]");
            thirdChoice.click();
            this.thinkTime(5000);
            WebElement firstCheckbox = this.getElement("//label[1]//input[@id='shppMainCd']");
            firstCheckbox.click();
            this.thinkTime(5000);
            WebElement thirdCheckbox = this.getElement("//label[3]//input[@id='shppMainCd']");
            thirdCheckbox.click();
            this.thinkTime(5000);
            WebElement checkbox1 = this.getElement("//label[1]//input[@id='shppDivDtlCd']");
            checkbox1.click();
            this.thinkTime(5000);
            WebElement checkbox2= this.getElement("//label[2]//input[@id='shppDivDtlCd']");
            checkbox2.click();
            this.thinkTime(5000);
            WebElement checkbox3 = this.getElement("//label[2]//input[@id='shppProgStatDtlCd']");
            checkbox3.click();
            this.thinkTime(5000);
            WebElement searchButton = this.getElement("//button[contains(@class, 'btn_point_ico')][contains(@id, 'searchBtn')]");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", searchButton);
            searchButton.click();
            this.thinkTime(30000);
            WebElement selectAll = this.getElement("//div[contains(@class, 'hdrcell')]//input[@type='checkbox']");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", selectAll);
            selectAll.click();
            this.thinkTime(15000);
            WebElement downloadByExcel = this.getFirstElement("//a[contains(@class, 'ico_excel _downExcelBtn')]");
            ((JavascriptExecutor)
                    webDriver).executeScript ("arguments[0].scrollIntoView();", downloadByExcel);
            downloadByExcel.click();
            this.thinkTime(20000);
        } catch (WebDriverException e) {
            logger.error("Failed to download file");
            Assert.fail("Failed to download file");
        }

    }
}
