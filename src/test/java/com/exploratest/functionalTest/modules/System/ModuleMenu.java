package com.exploratest.functionalTest.modules.System;

import com.exploratest.functionalTest.modules.BaseModule;
import com.exploratest.functionalTest.modules.RLCommon;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import static com.exploratest.functionalTest.modules.RLCommon.*;

public class ModuleMenu extends BaseModule {
    public ModuleMenu(WebDriver webDriver) {
        super(webDriver);
    }

    public void clickNavigationPaneIfShown() {
        String navigationPaneXpath = String.format("//div[@role='button']%s", getXpathContainsClassAttribute("modulesPane-opener"));
        try {
            if (this.isElementPresent(By.xpath(navigationPaneXpath))) {
                WebElement navigationPane = this.getElement(navigationPaneXpath, 5, false);
                if (navigationPane.isDisplayed()) {
                    clickElementIfNotExpanded(navigationPane);
                }
            }
        } catch (StaleElementReferenceException | TimeoutException ex) {
            // do nothing
        }
    }

    public void openModulesListIfNotExpanded() {
        WebElement modulesList = this.getElement("//div[@role='group'][@aria-label='Modules']");
        clickElementIfNotExpanded(modulesList);
    }

    public WebElement clickModuleItem(String dataDynTitle) {
        WebElement module = this.getElement(String.format("//a[@role='menuitem'][@data-dyn-title='%s']", dataDynTitle));
        if (module != null) {
            String classAttr = module.getAttribute("class");
            String[] classes = classAttr.split("\\s");

            // find the whole set if there exist the required CSS class
            boolean isActive = false;
            for (int i = 0; i < classes.length; i++) {
                if ("modulesPane-isActive".equals(classes[i])) {
                    isActive = true;
                }
            }

            // if the target module is not clicked, click it
            if (!isActive) {
                module.click();
            }
        }

        return module;
    }

    public WebElement openModule(String name) {
        openModulesListIfNotExpanded();
        return clickModuleItem(name);
    }

    public WebElement openAccountsPayableModule() {
        return openModule("Accounts payable");
    }

    public WebElement openAccountsReceivableModule() {
        return openModule("Accounts receivable");
    }

    public WebElement openCashAndBankManagementModule() {
        return openModule("Cash and bank management");
    }

    public WebElement openCommonModule() {
        return openModule("Common");
    }

    public WebElement openEPOModule() {
        return openModule("ePO");
    }

    public WebElement openFixedAssetsModule() {
        return openModule("Fixed assets");
    }

    public WebElement openGeneralLedgerModule() {
        return openModule("General ledger");
    }

    public WebElement openInterfaceModule() {
        return openModule("Interface");
    }

    public WebElement openInventoryManagementModule() {
        return openModule("Inventory management");
    }

    public WebElement openProcureAndSourcingModule() {
        return openModule("Procurement and sourcing");
    }

    public WebElement openProductInformationManagementModule() {
        return openModule("Product information management");
    }

    public WebElement openRetailModule() {
        return openModule("Retail");
    }

    public WebElement openSalesAndMarketingModule() {
        return openModule("Sales and marketing");
    }

    public WebElement ensureModuleContainerIsOpened() {
        WebElement modulesPaneFlyout = this.getElement(String.format("//div%s", getXpathContainsClassAttribute("modulesPane-flyout")));
        Assert.assertNotNull(modulesPaneFlyout, String.format("Cannot open module pane flyout"));
        return modulesPaneFlyout;
    }

    private WebElement clickSubModuleGroup(WebElement moduleContainer, String text) {
        WebElement subModule = this.getElement(String.format("//a%s[@data-dyn-title='%s']", getXpathContainsClassAttribute("modulesFlyout-LinkGroup"), text));
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.pollingEvery(1, TimeUnit.SECONDS);
        // By subModuleXpathSelector = By.xpath(String.format("//a%s[@data-dyn-title='%s']", this.getXpathContainsClassAttribute("modulesFlyout-LinkGroup"), text));
        // WebElement subModule = wait.until(ExpectedConditions.presenceOfElementLocated(subModuleXpathSelector));
        Assert.assertNotNull(subModule, String.format("Cannot open sub-module group %s", text));

        this.scrollElementInToView(subModule);

        if (getWebElementAriaExpandedAttribute(subModule)) {
            wait.until(ExpectedConditions.elementToBeClickable(subModule));
            subModule.click();
        }

        return subModule;
    }

    public WebElement openSubModuleGroup(String name) {
        WebElement moduleContainer = ensureModuleContainerIsOpened();
        return clickSubModuleGroup(moduleContainer, name);
    }

    public WebElement openSubSubModuleGroup(WebElement subModule, String name) {
        return clickSubModuleGroup(subModule, name);
    }

    public WebElement clickMenuItem(WebElement parentModule, String name) {
        WebElement menuItem = this.getElement(String.format("//a%s[@data-dyn-title='%s']", getXpathContainsClassAttribute("modulesFlyout-linkText"), name));
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.pollingEvery(1, TimeUnit.SECONDS);
        // By menuItemXpath = By.xpath(String.format("//a%s[@data-dyn-title='%s']", this.getXpathContainsClassAttribute("modulesFlyout-linkText"), name));
        // WebElement menuItem = wait.until(ExpectedConditions.presenceOfElementLocated(menuItemXpath));
        Assert.assertNotNull(menuItem, String.format("Cannot find menu item %s", name));

        this.scrollElementInToView(menuItem);
        wait.until(ExpectedConditions.elementToBeClickable(menuItem));
        menuItem.click();

        return menuItem;
    }
}
