package com.exploratest.unitTest.framework.utils;

import com.exploratest.functionalTest.framework.utils.ConfigUtil;
import org.springframework.util.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PropConfigUtilTest {


    @BeforeClass
    public void Setup() {
        System.setProperty("test_config_file","src/resources/rl.d365.test01.properties");
    }

    @Test
    public void testGetBrowserType() {
        Assert.isTrue(ConfigUtil.INSTANCE.getBrowserType().equals("chrome"),"incorrect browserType");
    }

    @Test
    public void testGetTestSet() {
        Assert.isTrue(ConfigUtil.INSTANCE.getTestSet().equals("src/resources/testcases/RLTestCases.xlsx"),"incorrect worksheet");
    }

    @Test
    public void testGetBaseURL() {
        Assert.isTrue(ConfigUtil.INSTANCE.getBaseURL().equals("https://rlas-test01aos.sandbox.ax.dynamics.com/"),"incorrect url");
    }

    @Test
    public void testGetChromeDriver() {
        Assert.isTrue(ConfigUtil.INSTANCE.getChromeDriver().equals("src/resources/drivers/Windows/chromedriver.exe"),"incorrect driver path");
    }

    @Test
    public void testScreenShotBeforeTest() {
        Assert.isTrue(ConfigUtil.INSTANCE.getScreenShotBeforeStep() == true,"no screenshot before test step");
    }


    @AfterClass
    public void tearDown() {
    }
}
