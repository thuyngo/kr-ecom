package com.exploratest.unitTest.framework.utils;

import com.exploratest.functionalTest.framework.utils.ExcelReader;
import org.springframework.util.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ExcelReaderTest {

    private ExcelReader xlReader = null;

    @BeforeMethod
    public void setUp() {
        System.out.println(String.format("Working Directory = %s", System.getProperty("user.dir")));
        xlReader = new ExcelReader("src/resources/testcases/RLTestCases.xlsx");
    }

    @Test
    public void testReadXL() {
        Assert.isTrue(xlReader.isSheetExist("Test Cases"), "Worksheet Test cases exists");
        Assert.isTrue(xlReader.isSheetExist("Item"), "Worksheet Item exists");
        Assert.isTrue(xlReader.getColumnCount("Test Cases") == 4, "Test Cases worksheet column count is correct");
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
        }
    }
}
