package com.exploratest.unitTest.framework.utils;

import com.exploratest.functionalTest.framework.utils.ConfigUtil;
import org.springframework.util.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class XLConfigUtilTest {

    @BeforeClass
    public void Setup() {

        System.setProperty("test_config_file","src/resources/rl.d365.test01.xlsx");
    }

    @Test
    public void testXLGetBrowserType() {
        Assert.isTrue(ConfigUtil.INSTANCE.getScreenShotAfterStep() == true,"not getting screenshot after test");
    }

    @Test
    public void testXLGetTestSet() {
        Assert.isTrue(ConfigUtil.INSTANCE.getTestSet().equals("src/resources/testcases/RLTestCases.xlsx"),"incorrect worksheet");
    }

    @Test
    public void testXLGetBaseURL() {
        Assert.isTrue(ConfigUtil.INSTANCE.getBaseURL().equals("https://rlas-test01aos.sandbox.ax.dynamics.com/"),"incorrect url");
    }

    @Test
    public void testXLGetExceDelay() {
        Assert.isTrue(ConfigUtil.INSTANCE.getExecDelay() == 100,"incorrect exec delay");
    }

    @AfterClass
    public void tearDown() {
    }
}