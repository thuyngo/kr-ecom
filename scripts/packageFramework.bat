@echo off

for /f "tokens=* delims=" %%d in (
  'Get-Date.exe "yyyyMMdd"'
) do set "today=%%d"

set destCab=RLErpAutoTestFramework%today%
cd ..

mkdir %destCab%\logs
echo destination directory: %destCab%

robocopy /NJS /NJH . %destCab% build.gradle
robocopy /NJS /NJH . %destCab% gradlew.bat     
robocopy /NJS /NJH . %destCab% runtest.bat     
robocopy /NJS /NJH . %destCab% settings.gradle 
robocopy /NJS /NJH . %destCab% testng.xml
robocopy /NJS /NJH . %destCab% runtest.bat
robocopy /NJS /NJH /S ./scripts %destCab%/scripts *.*
robocopy /NJS /NJH /S ./build/libs %destCab%/build/libs *.*
robocopy /NJS /NJH /S ./build/classes %destCab%/build/classes *.*
robocopy /NJS /NJH /S ./gradle %destCab%/gradle *.*
robocopy /NJS /NJH /S ./src/resources %destCab%/src/resources *.*

tools\7z.exe a -y %destCab%.zip %destCab%
echo File %destCab%.zip created
echo clean up temp files now
rd /S /Q %destCab% 

echo Copy and unzip the file on target machine for use
